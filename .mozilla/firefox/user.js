// Warn on closing multiple tabs
user_pref("browser.tabs.warnOnClose", true);

// UI layout
user_pref("browser.bookmarks.addedImportButton", true);
user_pref("browser.uiCustomization.state", "{\"placements\":{\"widget-overflow-fixed-list\":[],\"unified-extensions-area\":[],\"nav-bar\":[\"back-button\",\"forward-button\",\"stop-reload-button\",\"customizableui-special-spring1\",\"urlbar-container\",\"customizableui-special-spring2\",\"downloads-button\",\"unified-extensions-button\"],\"toolbar-menubar\":[\"menubar-items\"],\"TabsToolbar\":[\"tabbrowser-tabs\",\"new-tab-button\",\"alltabs-button\"],\"PersonalToolbar\":[\"personal-bookmarks\"]},\"seen\":[\"save-to-pocket-button\",\"developer-button\"],\"dirtyAreaCache\":[\"nav-bar\",\"PersonalToolbar\",\"toolbar-menubar\",\"TabsToolbar\"],\"currentVersion\":101,\"newElementCount\":4}");

// Clean new tab page
user_pref("browser.newtabpage.enabled", false);
user_pref("browser.startup.homepage", "about:blank");

// Strict tracking protection
user_pref("browser.contentblocking.category", "strict");
user_pref("network.http.referer.disallowCrossSiteRelaxingDefault.top_navigation", true);
user_pref("privacy.annotate_channels.strict_list.enabled", true);
user_pref("privacy.fingerprintingProtection", true);
user_pref("privacy.query_stripping.enabled", true);
user_pref("privacy.query_stripping.enabled.pbmode", true);
user_pref("privacy.trackingprotection.emailtracking.enabled", true);
user_pref("privacy.trackingprotection.enabled", true);
user_pref("privacy.trackingprotection.socialtracking.enabled", true);

// Do not track
user_pref("privacy.donottrackheader.enabled", true);
user_pref("privacy.globalprivacycontrol.enabled", true);

// Do not remember signons
user_pref("signon.rememberSignons", false);

// Disable Auto-fill
user_pref("extensions.formautofill.creditCards.enabled", false);

// Enable default private browsing
user_pref("browser.privatebrowsing.autostart", true);

// Disable telemetry
user_pref("app.shield.optoutstudies.enabled", false);
user_pref("datareporting.healthreport.uploadEnabled", false);

// HTTPS only mode
user_pref("dom.security.https_only_mode", true);

// DNS over HTTPS only mode
user_pref("doh-rollout.disable-heuristics", true);
user_pref("network.trr.mode", 3);
user_pref("network.trr.uri", "https://mozilla.cloudflare-dns.com/dns-query");

// Allow all notifications
// user_pref("permissions.default.desktop-notification", 1);

// Bookmarks
user_pref("browser.chrome.site_icons", false);
// docker run -ti --rm -v $(pwd):/apps -w /apps alpine/sqlite places.sqlite "UPDATE moz_bookmarks SET dateAdded = 1675460677000000 WHERE title = 'toolbar';"
//user_pref("browser.bookmarks.autoExportHTML", true);
//user_pref("browser.places.importBookmarksHTML", true);
//user_pref("browser.bookmarks.file",	'~/.mozilla/firefox/bookmarks/default.html');
